#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const username = process.env.USERNAME, password = process.env.PASSWORD;
    let noteUrl;

    let browser, app, cloudronName;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function registerAndLogin(email, password) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.css('.btn.ui-signin'));
        await browser.findElement(By.css('.btn.ui-signin')).click();

        await waitForElement(By.xpath('//input[@name="email"]'));
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(email);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.sleep(2000); // takes a bit for Register button to get enabled
        await browser.findElement(By.xpath('//button[contains(@formaction, "/register")]')).click();

        await waitForElement(By.xpath('//div[contains(text(), "successfully registered")]'));

        await waitForElement(By.xpath('//button[text()="Sign In"]'));
        await browser.findElement(By.xpath('//button[text()="Sign In"]')).click();
        await waitForElement(By.xpath('//input[@name="email"]'));
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(email);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.sleep(2000); // takes a bit for Register button to get enabled
        await browser.findElement(By.xpath('//button[contains(@formaction, "/login")]')).click();

        await waitForElement(By.xpath('//a[contains(., "New note")]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated=true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//div[@class="ui-signin"]'));

        await browser.findElement(By.xpath('//button[text()="Sign In"]')).click();
        await waitForElement(By.xpath(`//a[contains(., "Sign in via ${cloudronName}")]`));
        await browser.findElement(By.xpath(`//a[contains(., "Sign in via ${cloudronName}")]`)).click();

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//a[contains(., "New note")]'));
    }

    async function newNote() {
        await browser.get('https://' + app.fqdn + '/new');
        await browser.wait(until.elementLocated(By.xpath('//a[contains(text(), "Publish")]')), TEST_TIMEOUT);
        await browser.sleep(8000); // code mirror takes a while to load
        noteUrl = await browser.getCurrentUrl();
        console.log('The note url is ' + noteUrl);
        await browser.sleep(5000);
        await browser.findElement(By.css('.CodeMirror textarea')).sendKeys('hello cloudron');
        await browser.sleep(4000); // give it a second to 'save'
    }

    async function checkExistingNote() {
        await browser.get(noteUrl);
        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(), "hello cloudron")]')), TEST_TIMEOUT);
    }

    async function checkNoteIsEditablePermission() {
        await browser.get(noteUrl);
        await browser.sleep(3000);
        await waitForElement(By.xpath('//p[contains(text(), "hello cloudron")]'));
        await waitForElement(By.xpath('//a[@id="permissionLabel" and @title="Signed people can edit"]'));
        await browser.get('https://' + app.fqdn);
        await browser.sleep(3000);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//button[@id="profileLabel"]'));
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//button[@id="profileLabel"]')).click();
        await waitForElement(By.xpath('//a[contains(text(), "Sign Out")]'));
        await browser.findElement(By.xpath('//a[contains(text(), "Sign Out")]')).click();
        await browser.sleep(5000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can register and login', registerAndLogin.bind(null, 'test@cloudron.io', password));

    it('can create new note', newNote);
    it('can check existing note', checkExistingNote);
    it('can logout', logout);
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    it('install app (sso)', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, username, password, false));
    it('can create new note', newNote);
    it('can check existing note', checkExistingNote);
    it('can logout', logout);

    it('did create editable note', checkNoteIsEditablePermission);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });


    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('can check existing note', checkExistingNote);
    it('can logout', logout);

    it('did create editable note', checkNoteIsEditablePermission);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('can check existing note', checkExistingNote);
    it('can logout', logout);

    it('did create editable note', checkNoteIsEditablePermission);

    it('move to different location', function (done) {
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
        noteUrl = noteUrl.replace(LOCATION, LOCATION + '2');

        done();
    });

    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('can check existing note', checkExistingNote);
    it('can logout', logout);

    it('did create editable note', checkNoteIsEditablePermission);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install from appstore', function () {
        execSync('cloudron install --appstore-id io.hackmd.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });

    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('can create new note', newNote);
    it('can logout', logout);

    it('can update', function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        getAppInfo();
    });

    it('can login OIDC', loginOIDC.bind(null, username, password, true));
    it('can check existing note', checkExistingNote);
    it('can logout', logout);

    it('did create editable note', checkNoteIsEditablePermission);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
