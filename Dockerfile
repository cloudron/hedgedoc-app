FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

WORKDIR /app/code

# renovate: datasource=github-releases depName=hedgedoc/hedgedoc versioning=semver
ARG HEDGEDOC_VERSION=1.10.2

# https://docs.hedgedoc.org/setup/manual-setup/
RUN curl -L https://github.com/hedgedoc/hedgedoc/releases/download/${HEDGEDOC_VERSION}/hedgedoc-${HEDGEDOC_VERSION}.tar.gz | tar -xz --strip-components 1 -f -

RUN yarn config set --home enableTelemetry 0 && \
    yarn install --immutable

# Despite HMD_TMP_PATH, pdf generation seems to use /app/code/tmp
RUN rm -rf /app/code/public/uploads && ln -sf /app/data/uploads /app/code/public/uploads && \
    rm -rf /app/code/tmp && ln -sf /tmp/codimd /app/code/tmp

# picks up the correct section in config.json (https://docs.hedgedoc.org/configuration/)
ENV NODE_ENV=production

COPY config.json.template start.sh /app/pkg/
RUN ln -sfn /app/data/config.json /app/code/config.json

CMD ["/app/pkg/start.sh"]
