[0.1.0]
* Initial version
* Made possible by @dictcp, @felix and @jeau

[0.2.0]
* Update screenshot and add documentation link

[0.3.0]
* HackMD is now CodiMD

[1.0.0]
* Show full title by hovering over to table of contents entries
* Add generic OAUTH2 support for authentication
* Redirect unauthenticated user to login page on "forbidden" pages
* Add ability to add ToS and privacy documents without code changes
* Add account deletion as part of user self-management
* Add download of all own notes
* Add privacy policy example (no legal advice)
* Increase checkbox size on slides
* Add support for Azure blob storage for image uploads
* Add Korean translation
* Add note about official K8s chart for deployment
* Add toolbar for markdown shortcuts in editor
* Add ability to disable Gravatar integration
* Add print icon to slide menu which leads to the print view.
* Add sequelize to setup instructions
* Update various packages

[1.0.1]
* Update CodiMD to 1.2.1

[1.1.0]
* Use latest base image

[1.1.1]
* [Disable use of CDN](https://github.com/hackmdio/codimd/issues/1111)

[1.2.0]
* Update CodiMD to 1.3.0

[1.2.1]
* Update CodiMD to 1.3.1
* Add Serbian language
* Fix broken redirect for empty serverURL
* Fix wrong variable type for HSTS maxAge
* Fix GitLab snippets showing up without being configured
* Fix broken PDF export

[1.3.0]
* Update CodiMD to 1.3.2
* The CodiMD project is now running in it's [own organization](https://github.com/codimd) and has made a hard fork from the upstream HackMD project. You can read up on this topic [here](https://github.com/hackmdio/codimd/issues/1170).
* Cloudron will continue to follow the CodiMD project. A HackMD package is possible in the future but this very much depends on what the [HackMD project decides](https://github.com/hackmdio/codimd/issues/1170#issuecomment-477718380).

[1.4.0]
* Update CodiMD to 1.4.0
* Use libravatar instead of Gravatar
* Fix language description capitalization
* Move upload button into the toolbar
* Clean up Heroku configurations
* Add new screenshot to README and index page
* Update languages (pl, sr, zh-CN, fr, it, ja, zh-TW, de, sv, es)
* Add vietnamese language

[1.5.0]
* Update manifest to v2
* Prepare for uid re-map

[1.6.0]
* Newer installations now use "username" as the uid for easier portability

[1.7.0]
* Update CodiMD to 1.5.0
* Disabling PDF export due to security problems
* Add functionality to respect Do-Not-Track header
* Add Arabian translation
* Fix styling in slide preview
* Fix some lint warning
* Upgrade Sequelize to version 5
* Add Linuxserver.io setup instructions for CodiMD
* Update translations for DE, SV, ID
* Add ability to upload SVGs
* Add dbURLconfig as docker secret
* Upgrade meta-marked - Fixes DOS capability in CodiMD (ba6a24a)
* Fix variable names in docker secrets config library

[1.7.1]
* Make the image upload backend configurable

[1.8.0]
* Update CodiMD to 1.6.0
* Add ability to create note based on alias in free-url-mode
* Add security note describing the preferred way for responsible disclosures
* Extend forbiddenNoteIds to prevent conflicts with resource directories
* Add OpenGraph metadata support
* Add slovak language

[1.9.0]
* Use latest base image 2.0.0

[1.10.0]
* Add forum url in manifest

[1.11.0]
* CodiMD is now HedgeDoc
* Update HedgeDoc to 1.7.0
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.7.0)
* Improvements to our cookie handling
* Compatibility with Node 14
* Translation updates
* Various dependency updates

[1.11.1]
* Update HedgeDoc to 1.7.1
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.7.1)
* CVE-2020-26286: Arbitrary file upload
* CVE-2020-26287: Stored XSS in mermaid diagrams

[1.11.2]
* Update HedgeDoc to 1.7.2
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.7.2)
* CVE-2021-21259: Stored XSS in slide mode - An attacker can inject arbitrary JavaScript into a HedgeDoc note.

[1.12.0]
* Update base image to v3

[1.12.1]
* Rename HMD env vars to CMD
* Move package files to `/app/pkg`

[1.13.0]
* Update HedgeDoc to 1.8.0
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.8.0)
* CVE-2021-29474: Relative path traversal Attack on note creation
* Removed dependency on external imgur library
* HTML language tags are now set up in a way that stops Google Translate from translating note contents while editing
* Removed yahoo.com from the default content security policy
* New translations for Bulgarian, Persian, Galician, Hebrew, Hungarian, Occitan and Brazilian Portuguese
* Updated translations for Arabic, English, Esperanto, Spanish, Hindi, Japanese, Korean, Polish, Portuguese, Turkish and Traditional Chinese
* CVE-2021-21306: Underscore ReDoS in the marked library

[1.13.1]
* Update HedgeDoc to 1.8.1
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.8.1)
* Improve behavior of the 'Quote', 'List', 'Unordered List' and 'Check List' buttons in the editor to automatically apply to the complete first and last line of the selection
* Fix click handler for numbered task

[1.13.2]
* Update HedgeDoc to 1.8.2
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.8.2)
* CVE-2021-29503: Improper Neutralization of Script-Related HTML Tags in Notes
* Fix a potential XSS-vector in the handling of usernames and profile pictures

[1.14.0]
* Change default note permission for new installs to be `editable` (matches upstream defaults)

[1.15.0]
* Update HedgeDoc to 1.9.0
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.9.0)
* CVE-2021-39175: XSS vector in slide mode speaker-view
* This release removes Google Analytics and Disqus domains from our default Content Security Policy, because they were repeatedly used to exploit security vulnerabilities.
If you want to continue using Google Analytics or Disqus, you can re-enable them in the config.

[1.15.1]
* Update HedgeDoc to 1.9.2
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.9.1)
* Add workaround for incorrect CSP handling in Safari
* Fix crash when an unexpected response from the GitLab API is encountered
* Fix crash when using hungarian language

[1.15.2]
* Update base image to 3.2.0

[1.15.3]
* Update HedgeDoc to 1.9.3
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.9.3)
* Fix Enumerable upload file names
* Libravatar avatars render as ident-icons when no avatar image was uploaded to Libravatar or Gravatar
* Add database connection error message to log output
* Allow SAML authentication provider to be named
* Suppress error message when git binary is not found

[1.15.4]
* Update HedgeDoc to 1.9.4
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.9.4)
* Remove unexpected shell call during migrations
* More S3 config options: upload folder & public ACL (thanks to @lautaroalvarez)

[1.15.5]
* Change `allowFreeUrl` to `allowFreeURL` in default config

[1.15.6]
* Update HedgeDoc to 1.9.5
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.9.5)
* Add dark mode toggle in mobile view
* Replace embedding shortcode regexes with more specific ones to safeguard against XSS attacks

[1.15.7]
* Update HedgeDoc to 1.9.6
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.9.6)
* Fix migrations deleting all notes when SQLite is used

[1.16.0]
* Update base image to 4.0.0

[1.16.1]
* Update HedgeDoc to 1.9.7
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.9.7)
* Fix note titles with special characters producing invalid file names in user export zip file
* Fix night-mode toggle not working when page is loaded with night-mode enabled

[1.16.2]
* Update HedgeDoc to 1.9.8
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.9.8)
* Extend boolean environment variable parsing with other positive answers and case insensitivity
* Allow setting of documentMaxLength via CMD_DOCUMENT_MAX_LENGTH environment variable (contributed by @jmallach)
* Add dedicated healthcheck endpoint at /_health that is less resource intensive than /status
* Compatibility with Node.js 18 and later
* Add a config option to disable the /status and /metrics endpoints

[1.16.3]
* Update HedgeDoc to 1.9.9
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.9.9)
* CVE-2023-38487: API allows to hide existing notes

[1.17.0]
* Implement OIDC auth

[1.18.0]
* Update base image to 4.2.0

[1.19.0]
* Implement optionalSso

[1.20.0]
* Update HedgeDoc to 1.10.0
* [Full changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.10.0)
* GHSA-pjf2-269h-cx7p: MySQL & free URL mode allows to hide existing notes
* Add disableNoteCreation config option for read-only instances
* Add a pointer to Mermaid 9.1.7 documentation, which is what HedgeDoc 1 supports.
* Compatibility with Node.js 22 is now checked in CI
* Fix a crash when having numeric-only values in opengraph frontmatter
* Fix unnecessary session creation on healthcheck endpoint
* Fix invalid metadata being sent for minio uploads
* Fix screen readers announcing headings twice
* Fix a crash when receiving unexpected OAuth profile data
* Fix some cases of HedgeDoc not redirecting to the previous page after login
* Fix heading anchor links referencing an invalid URL
* Our meta-marked package is now published to NPM, fixing some installation issues

[1.20.1]
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[1.20.2]
* Update hedgedoc to 1.10.1
* [Full Changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.10.1)
* Add fixed rate-limiting to the login and register endpoints
* Add configurable rate-limiting to the new notes endpoint
* Fix a crash when cannot read user profile in OAuth ([#&#8203;5850](https://github.com/hedgedoc/hedgedoc/pull/5850) by [@&#8203;lautaroalvarez](https://github.com/lautaroalvarez))
* Fix CSP Header for mermaid embedded images ([#&#8203;5887](https://github.com/hedgedoc/hedgedoc/pull/5887) by [@&#8203;domrim](https://github.com/domrim))
* Change default of HSTS preload to false for compliance with the HSTS preload list requirements ([#&#8203;5913](https://github.com/hedgedoc/hedgedoc/issues/5913) by [@&#8203;SvizelPritula](https://github.com/SvizelPritula))
* [Dominik Rimpf](https://github.com/domrim)
* [Lautaro Alvarez](https://github.com/lautaroalvarez)

[1.20.3]
* Update hedgedoc to 1.10.2
* [Full Changelog](https://github.com/hedgedoc/hedgedoc/releases/tag/1.10.2)
* Check if a valid user id is present when using OAuth2
* Abort SAML login if NameID is undefined instead of logging in with a user named "undefined" (Thanks [@&#8203;Haanifee](https://github.com/Haanifee))
* Set default values for username and email attribute mapping in SAML configuration

