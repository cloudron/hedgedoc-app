#!/bin/bash

set -eu

# prepare data directory
mkdir -p /app/data/uploads /tmp/codimd /run/codimd

if [ ! -e /app/data/config.json ]; then
    echo "=> Creating initial template on first run"
	cp /app/pkg/config.json.template /app/data/config.json
fi

# generate and store an unique sessionSecret for this installation
CONFIG_JSON=/app/data/config.json
if [ $(jq .production.sessionSecret ${CONFIG_JSON}) == "null" ]; then
    echo "=> generating sessionSecret"
    sessionsecret=$(pwgen -1sc 32)
    jq ".production.sessionSecret = \"$sessionsecret\"" ${CONFIG_JSON} | sponge ${CONFIG_JSON}

    if [[ -z "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        echo "=> enabling email login"
        jq ".production.allowEmailRegister = true" ${CONFIG_JSON} | sponge ${CONFIG_JSON}
        jq ".production.email = true" ${CONFIG_JSON} | sponge ${CONFIG_JSON}
    fi
fi

# these cannot be changed by user (https://docs.hedgedoc.org/configuration/). env vars take precedence over config.json
export CMD_DOMAIN="${CLOUDRON_APP_DOMAIN}"
export CMD_PROTOCOL_USESSL=true
export CMD_DB_URL="${CLOUDRON_POSTGRESQL_URL}"

export CMD_PORT=3000
export CMD_TMP_PATH=/tmp/codimd

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    # https://docs.hedgedoc.org/guides/auth/authelia/
    echo "=> configuring OIDC"
    export CMD_OAUTH2_PROVIDERNAME=${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}
    export CMD_OAUTH2_CLIENT_ID="${CLOUDRON_OIDC_CLIENT_ID}"
    export CMD_OAUTH2_CLIENT_SECRET="${CLOUDRON_OIDC_CLIENT_SECRET}"
    export CMD_OAUTH2_SCOPE="openid email profile"
    export CMD_OAUTH2_USER_PROFILE_USERNAME_ATTR=sub
    export CMD_OAUTH2_USER_PROFILE_DISPLAY_NAME_ATTR=name
    export CMD_OAUTH2_USER_PROFILE_EMAIL_ATTR=email
    export CMD_OAUTH2_BASE_URL="${CLOUDRON_OIDC_ISSUER}"
    export CMD_OAUTH2_USER_PROFILE_URL="${CLOUDRON_OIDC_PROFILE_ENDPOINT}"
    export CMD_OAUTH2_TOKEN_URL="${CLOUDRON_OIDC_TOKEN_ENDPOINT}"
    export CMD_OAUTH2_AUTHORIZATION_URL="${CLOUDRON_OIDC_AUTH_ENDPOINT}"
fi

echo "=> Changing permissions"
chown -R cloudron:cloudron /app/data /tmp/codimd /run/codimd

echo "=> Starting HedgeDoc"
exec /usr/local/bin/gosu cloudron:cloudron node app.js

